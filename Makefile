all: assignment4

assignment4: main.o graph.o
	g++ main.o graph.o -o assignment4

main.o: graph.h main.cpp
	g++ -c graph.h main.cpp

graph.o: graph.h graph.cpp
	g++ -c graph.h graph.cpp

clean:
	rm -rf *.o
	rm -rf *.gch

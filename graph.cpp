#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "graph.h"



Edge::Edge(int org, int dest){
    
        origin = org;
        destination = dest;
    }

int Edge::getOrigin() {
	return origin;
}

int Edge::getDestination() {
	return destination;
}


Vertex::Vertex(int id) {		// change to int

    name = id;
    label = -1;
}

void Vertex::addEdge(int v){

    Edge newEdge(name, v);
    edges.push_back(newEdge);
}

void Vertex::changeLabel(int lab) {
    label = lab;
}

int Vertex::getName() {
	return name;
} 

int Vertex::getLabel() {
    return label;
}

vector<Edge> Vertex::getEdges() {
	return edges;
}

/*
    Creates a linked list like graph of the size of nodes
    Number of edge will always equal the 2 * nodes

    Can probably move this function to the Graph class
*/
struct Graph createLinkedGraph(int nodes) {
    struct Graph g;
    Vertex v = Vertex(0);
    // put first node into graph
    g.graph.push_back(v);
    int i = 1;
    for(i = 1; i < nodes; i++) {
            Vertex k = Vertex(i);
            g.graph.push_back(k);
            // add edge from previous node to current node
            g.graph[i-1].addEdge(g.graph[i].getName());
            // add edge from current node to previous node
            g.graph[i].addEdge(g.graph[i-1].getName());
    }

    // add edge from last node to the first node
    g.graph[i-1].addEdge(g.graph[0].getName());
    // add edge from the first node to the las node
    g.graph[0].addEdge(g.graph[i-1].getName());

    /*
    // Debug stuff
    for(int i = 0; i < nodes; i++) {
        Vertex v = g.graph[i];
        vector<Edge>::iterator it;
        // get edges
        vector<Edge> edges = v.getEdges();
        it = edges.begin();
        while(it != edges.end()) {
            int num = it->getDestination();
            int num2 = g.graph[i].getName();
            printf("Node %i has edge to %i\n", num2, num);
            it++;
        }
    }
    */
    g.edges = 2 * nodes;
    g.nodes = nodes;

    return g;
}
    /* 
    Creates a dense graph, or one that is half connected

    Number of edges varies

    Can probably move this function to the Graph class

*/
struct Graph createDenseGraph(int nodes) {
    struct Graph dg;
    // Create Graph with given number of nodes
    for(int i = 0; i < nodes; i++) {
        Vertex v = Vertex(i);
        dg.graph.push_back(v);
    }

    // Give 50/50 chance to add edge to each node in the graph
    dg.edges = 0;
    dg.nodes = nodes;
    srand(time(0));
    // Size of vertex is going to be the size of nodes anyway
    for(int i = 0; i < nodes; i++) {
        for(int j = 0 ; j < nodes; j++) {
            if(i != j) {
                int ran = rand() % 2;
                // if 0, add edge; else don't do anything
                if(ran == 0) {
                    dg.graph[i].addEdge(dg.graph[j].getName());
                    dg.edges++;
                }
            }
        }
    }

    /*
    // Debug stuff
    for(int i = 0; i < nodes; i++) {
        Vertex v = dg.graph[i];
        vector<Edge>::iterator it;
        // get edges
        vector<Edge> edges = v.getEdges();
        it = edges.begin();
        while(it != edges.end()) {
            int num = it->getDestination();
            int num2 = dg.graph[i].getName();
            printf("Node %i has edge to %i\n", num2, num);
            it++;
        }
    }
    */

    return dg;
}
